import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    DEBUG = False
    TESTING = False
    LOG_PATH = "logs/app.log"
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    SECRET_KEY = "change this secret key"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Development(Config):
    DEBUG = True
    TEMPLATES_AUTO_RELOAD = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(
        os.path.join(basedir, "db/db.sqlite")
    )


class Testing(Config):
    DEBUG = False
    TESTING = True
    LOG_PATH = "logs/test.log"


class Production(Config):
    LOG_PATH = os.getenv("LOG_PATH")
    SQLALCHEMY_DATABASE_URI = os.getenv(
        "SQLALCHEMY_DATABASE_URI"
    )
    SECRET_KEY = os.getenv("SECRET_KEY")


environment_config = {
    "development": Development,
    "testing": Testing,
    "production": Production,
}
