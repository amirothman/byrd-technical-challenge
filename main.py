import os
from app import create_app

environment = os.getenv("ENVIRONMENT")
app = create_app(environment)


if __name__ == "__main__":
    app.run()
