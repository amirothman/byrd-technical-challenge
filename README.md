# Byrd Technical Challenge

This code repository is a submission for the Byrd Technical Challenge.
Refer to [link](https://gist.github.com/evenicoulddoit/eaedb0e6d793d94099ef18cf02021a96).


## Development

- This code is developed with Python 3 using the Flask framework with additional extensions including:
    - flask_marshmallow: For se(de)rialization
    - flask_migrate: Automated migration creation
    - flask_sqlalchemy: SQL ORM
- Additionally, this code also uses:
    - gunicorn - webserver
    - schema - for validation
    - pandas for data manipulation
    - pytest - code testing
    - flake8 - linting
    - ipdb - debugging

## Running

### Setup

Create and activate virtual environment:

    python3 -m venv venv
    source venv/bin/activate

While virtual environment is active install the required modules:

    pip install -r requirements.txt

If we want to run the application in development
mode, we need to create some additional folders in root directory:

    mkdir logs
    mkdir db

Create the database for the application, under development mode:

    FLASK_APP=main.py ENVIRONMENT=development flask db upgrade

under production mode:

    FLASK_APP=main.py ENVIRONMENT=production \
    LOG_PATH=<path-to-log-directory> \
    SQLALCHEMY_DATABASE_URI=<database-uri> \
    SECRET_KEY=<secret-key> \
    flask db upgrade

Note to substitute the environment variables accordingly

### Application

To run the code in development mode, run the
following command in the root directory:

    ENVIRONMENT=development gunicorn main:app

to run in production:

    ENVIRONMENT=production \
    LOG_PATH=<path-to-log-directory> \
    SQLALCHEMY_DATABASE_URI=<database-uri> \
    SECRET_KEY=<secret-key> \
    gunicorn main:app

Note to substitute the environment variables accordingly

### Tests

To run the tests, while the virtual environment is active:

    pytest

## Endpoints

### SKU

GET /sku/

returns:

    [
        {
            "id": "integer",
            "product_name": "string",
            "created_at": "string",
            "updated_at": "string"
            
        }, ...
    ]


POST /sku/

request body:

    {
        "product_name": "string"
    }

returns:

    {
        "id": "integer",
        "product_name": "string",
        "created_at": "string",
        "updated_at": "string"
        
    }

GET /sku/{id}

returns:

    {
        "id": "integer",
        "product_name": "string",
        "created_at": "string",
        "updated_at": "string"
        
    }

PUT /sku/{id}

request body:

    {
        "product_name": "string" (optional)
        
    }

returns:

    {
        "id": "integer",
        "product_name": "string",
        "created_at": "string",
        "updated_at": "string"
        
    }

DELETE /sku/{id}

returns empty string

### Storage

GET /storages/

returns:

    [
        {
            "id": "integer",
            "sku_id": "integer",
            "sku": "product_name",
            "stock": "integer",
            "created_at": "string",
            "updated_at": "string"
            
        }, ...
    ]


POST /storages/

request body:

    {
        "sku_id": "integer",
        "stock": "integer",

    }

returns:

    {
        "id": "integer",
        "sku_id": "integer",
        "sku": "product_name",
        "stock": "integer",
        "created_at": "string",
        "updated_at": "string"  
    }

GET /storages/{id}

returns:

    {
        "id": "integer",
        "sku_id": "integer",
        "sku": "product_name",
        "stock": "integer",
        "created_at": "string",
        "updated_at": "string"  
    }

PUT /storages/{id}

request body:

    {
        "sku_id": "integer" (optional),
        "stock": "integer" (optional),

    }

returns:

    {
        "id": "integer",
        "sku_id": "integer",
        "sku": "product_name",
        "stock": "integer",
        "created_at": "string",
        "updated_at": "string"  
    }

DELETE /orders/{id}

returns empty string

### Orders

GET /orders/

returns:

    [
        {
            "id": "integer",
            "customer_name": "string",
            "created_at": "string",
            "updated_at": "string"
            
        }, ...
    ]


POST /orders/

request body:

    {
        "customer_name": "string"
    }

returns:

    {
        "id": "integer",
        "customer_name": "string",
        "created_at": "string",
        "updated_at": "string"
        
    }

GET /orders/{id}

returns:

    {
        "id": "integer",
        "customer_name": "string",
        "created_at": "string",
        "updated_at": "string"
        
    }

PUT /orders/{id}

request body:

    {
        "customer_name": "string"
        
    }

returns:

    {
        "id": "integer",
        "customer_name": "string",
        "created_at": "string",
        "updated_at": "string"
        
    }

DELETE /orders/{id}

returns empty string

### OrderLines

GET /order_lines/

returns:

    [
        {
            "id": "integer",
            "sku_id": "integer",
            "sku": "string",
            "quantity": "integer"
            "created_at": "string",
            "updated_at": "string"
            
        }, ...
    ]


POST /order_lines/

request body:

    {
        "sku_id": "integer",
        "quantity": "integer"
    }

returns:

    {
        "id": "integer",
        "sku_id": "integer",
        "sku": "string",
        "quantity": "integer"
        "created_at": "string",
        "updated_at": "string"
    }

GET /order_lines/{id}

returns:

    {
        "id": "integer",
        "sku_id": "integer",
        "sku": "string",
        "quantity": "integer"
        "created_at": "string",
        "updated_at": "string"
    }

PUT /order_lines/{id}

request body:

    {
        "sku_id": "integer" (optional),
        "quantity": "integer" (optional)
    }

returns:

    {
        "id": "integer",
        "sku_id": "integer",
        "sku": "string",
        "quantity": "integer"
        "created_at": "string",
        "updated_at": "string"
    }

DELETE /order_lines/{id}

returns empty string

### Fulfillments

Given the following;

GET /storages

returns:

    [
        {
            "created_at": "2019-11-27T10:11:43.678897",
            "id": 1,
            "sku": "aaa",
            "sku_id": 1,
            "stock": 5,
            "updated_at": "2019-11-27T10:11:43.678905"
        },
        {
            "created_at": "2019-11-27T10:11:52.880541",
            "id": 2,
            "sku": "bbb",
            "sku_id": 2,
            "stock": 5,
            "updated_at": "2019-11-27T10:11:52.880548"
        },
        {
            "created_at": "2019-11-27T10:11:56.312183",
            "id": 3,
            "sku": "bbb",
            "sku_id": 2,
            "stock": 10,
            "updated_at": "2019-11-27T10:11:56.312190"
        }
    ]

And the following;

GET /order_lines

returns:

    [
        {
            "created_at": "2019-11-27T10:14:06.901928",
            "id": 1,
            "quantity": 3,
            "sku": "aaa",
            "sku_id": 1,
            "updated_at": "2019-11-27T10:14:06.901934"
        },
        {
            "created_at": "2019-11-27T10:14:23.037902",
            "id": 2,
            "quantity": 15,
            "sku": "bbb",
            "sku_id": 2,
            "updated_at": "2019-11-27T10:14:23.037911"
        }
    ]

GET /fulfillments

Returns the pick needed to fulfill the order.

    [                                              
        {              
            "id": 1,              
            "quantity": 3              
        },                 
        {                                         
            "id": 2,
            "quantity": 5
        },                                                                                  
        {                
            "id": 3, 
            "quantity": 10
        }                         
    ]   

"id", refers to the storage id

## Order searching

Order searching is not implemented, below is how I would implement
this functionality:

- Create an additional table that serves as an index
- This index willl hold the following;
    - normalized_token
    - order_id
- Whenever an order is made, the name would be split by whitespace into tokens, e.g:
    - "Thomas Mustermann" to ["Thomas", "Mustermann"]
- Each token will be normalized by:
    - converting special characters, e.g:
        - ä to a
        - ß to s
    - lowercasing
- Each normalized token is inserted as a row in the index table, using the:
    - token as normalized_token
    - id of the Order as order_id
- When there is an incoming search query, the query will go through the same process of splitting by whitespace and normalizing.
- Use each normalized query token to look up from the index table and return
all the order_id's
- Return the orders with the matched order_id's