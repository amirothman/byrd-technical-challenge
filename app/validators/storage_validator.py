from schema import (
    Schema, And, Optional,
    SchemaMissingKeyError,
    SchemaWrongKeyError,
    SchemaUnexpectedTypeError,
    SchemaError
)

stock_rules = And(
    int,
    lambda x: x >= 0
)
sku_id_rules = And(
    int,
    lambda x: x > 0
)

storage_schema = Schema({
    "stock": stock_rules,
    "sku_id": sku_id_rules
})

update_storage_schema = Schema({
    Optional("stock"): stock_rules,
    Optional("sku_id"): sku_id_rules
})


def validate_storage(payload):
    try:
        storage_schema.validate(payload)
    except SchemaMissingKeyError as err:
        return {"error": str(err)}
    except SchemaWrongKeyError as err:
        return {"error": str(err)}
    except SchemaUnexpectedTypeError as err:
        return {"error": str(err)}
    except SchemaError as err:
        return {"error": str(err)}


def validate_storage_update(payload):
    try:
        update_storage_schema.validate(payload)
    except SchemaMissingKeyError as err:
        return {"error": str(err)}
    except SchemaWrongKeyError as err:
        return {"error": str(err)}
    except SchemaUnexpectedTypeError as err:
        return {"error": str(err)}
    except SchemaError as err:
        return {"error": str(err)}
