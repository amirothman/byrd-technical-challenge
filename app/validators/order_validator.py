from schema import (
    Schema, Use, Regex, And,
    SchemaMissingKeyError,
    SchemaWrongKeyError,
    SchemaUnexpectedTypeError,
    SchemaError
)

order_schema = Schema({
    "customer_name": And(
        Use(str),
        Regex(r"^.{1,200}$")
    )
})


def validate_order(payload):
    try:
        order_schema.validate(payload)
    except SchemaMissingKeyError as err:
        return {"error": str(err)}
    except SchemaWrongKeyError as err:
        return {"error": str(err)}
    except SchemaUnexpectedTypeError as err:
        return {"error": str(err)}
    except SchemaError as err:
        return {"error": str(err)}
