from schema import (
    Schema, And,
    Optional, SchemaMissingKeyError,
    SchemaWrongKeyError,
    SchemaUnexpectedTypeError,
    SchemaError
)

quantity_rules = And(
    int,
    lambda x: x >= 0
)
sku_id_rules = And(
    int,
    lambda x: x > 0
)

order_line_schema = Schema({
    "sku_id": sku_id_rules,
    "quantity": quantity_rules
})

update_order_line_schema = Schema({
    Optional("sku_id"): sku_id_rules,
    Optional("quantity"): quantity_rules
})


def validate_order_line(payload):
    try:
        order_line_schema.validate(payload)
    except SchemaMissingKeyError as err:
        return {"error": str(err)}
    except SchemaWrongKeyError as err:
        return {"error": str(err)}
    except SchemaUnexpectedTypeError as err:
        return {"error": str(err)}
    except SchemaError as err:
        return {"error": str(err)}


def validate_order_line_update(payload):
    try:
        update_order_line_schema.validate(payload)
    except SchemaMissingKeyError as err:
        return {"error": str(err)}
    except SchemaWrongKeyError as err:
        return {"error": str(err)}
    except SchemaUnexpectedTypeError as err:
        return {"error": str(err)}
    except SchemaError as err:
        return {"error": str(err)}
