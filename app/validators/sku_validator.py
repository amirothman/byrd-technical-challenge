from schema import (
    Schema, Use,
    SchemaMissingKeyError,
    SchemaWrongKeyError,
    SchemaUnexpectedTypeError,
    SchemaError
)

sku_schema = Schema({
    "product_name": Use(str)
})


def validate_sku(payload):
    try:
        sku_schema.validate(payload)
    except SchemaMissingKeyError as err:
        return {"error": str(err)}
    except SchemaWrongKeyError as err:
        return {"error": str(err)}
    except SchemaUnexpectedTypeError as err:
        return {"error": str(err)}
    except SchemaError as err:
        return {"error": str(err)}
