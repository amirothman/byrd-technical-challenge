from app import db
from .base import BaseModel


class Order(BaseModel):
    customer_name = db.Column(
        db.String(200), unique=True
    )

    def __repr__(self):
        return "<Order {}>".format(
            self.id
        )
