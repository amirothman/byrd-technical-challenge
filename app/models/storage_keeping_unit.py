from app import db
from .base import BaseModel


class StorageKeepingUnit(BaseModel):
    product_name = db.Column(
        db.String(200), unique=True
    )
    order_lines = db.relationship(
        "OrderLine",
        backref="storage_keeping_unit",
        lazy=True
    )
    storages = db.relationship(
        "Storage",
        backref="storage_keeping_unit",
        lazy=True
    )

    def __repr__(self):
        return "<StorageKeepingUnit {}>".format(
            self.id
        )
