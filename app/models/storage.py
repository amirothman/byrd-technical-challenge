from app import db
from .base import BaseModel


class Storage(BaseModel):
    stock = db.Column(
        db.Integer(), default=0
    )
    storage_keeping_unit_id = db.Column(
        db.Integer(),
        db.ForeignKey("storage_keeping_unit.id"),
        nullable=False
    )

    def __repr__(self):
        return "<Storage {}>".format(
            self.id
        )

    @property
    def sku(self):
        return self.storage_keeping_unit.product_name
