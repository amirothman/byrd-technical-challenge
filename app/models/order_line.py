from app import db
from .base import BaseModel


class OrderLine(BaseModel):
    storage_keeping_unit_id = db.Column(
        db.Integer(),
        db.ForeignKey("storage_keeping_unit.id"),
        nullable=False
    )
    quantity = db.Column(
        db.Integer(), default=0
    )

    def __repr__(self):
        return "<OrderLine {}>".format(
            self.id
        )

    @property
    def sku(self):
        return self.storage_keeping_unit.product_name
