from flask import (
    Blueprint,
    jsonify
)
import pandas as pd
from app import db
from app.models import Storage, OrderLine

fulfillments = Blueprint("fulfillments", __name__)


def can_order_be_fulfilled():
    order_query = db.session.query(
        OrderLine.storage_keeping_unit_id.label("sku_id"),
        db.func.sum(OrderLine.quantity).label("order_sum"),
    ).group_by(
        OrderLine.storage_keeping_unit_id
    )
    order_df = pd.DataFrame(order_query.all())
    storage_query = db.session.query(
        Storage.storage_keeping_unit_id.label("sku_id"),
        db.func.sum(Storage.stock).label("stock_sum")
    ).group_by(
        Storage.storage_keeping_unit_id
    )
    storage_df = pd.DataFrame(storage_query.all())
    result_df = pd.merge(order_df, storage_df, how='outer', on=['sku_id'])
    order_stock_comp = result_df.order_sum > result_df.stock_sum
    if result_df.stock_sum.isnull().any() or order_stock_comp.any():
        return jsonify({
            "error":
            "Quantity of order more than stock"
        }), 400


def compute_picks():
    query = db.session.query(
        OrderLine.storage_keeping_unit_id,
        db.func.sum(OrderLine.quantity),
    ).group_by(
        OrderLine.storage_keeping_unit_id
    )
    picks = []
    for sku_id, sum_quantity in query.all():

        sub_query = db.session.query(
            Storage
        ).filter(
            Storage.storage_keeping_unit_id == sku_id
        ).order_by(
            Storage.stock.asc()
        )

        for storage in sub_query.all():
            if sum_quantity > storage.stock:
                picks.append({
                    "id": storage.id,
                    "quantity": storage.stock
                })
                sum_quantity -= storage.stock
            else:
                picks.append({
                    "id": storage.id,
                    "quantity": sum_quantity
                })
                break

    return jsonify(picks)


@fulfillments.route("/fulfillments")
def index():
    error = can_order_be_fulfilled()
    if error:
        return error

    return compute_picks()
