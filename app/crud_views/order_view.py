from app.models import Order
from app.validators import validate_order
from app import ma
from .crud_view import CRUDView


class OrderSchema(ma.ModelSchema):

    class Meta:
        model = Order
        fields = ("id", "customer_name", "created_at", "updated_at")


order_schema = OrderSchema()


class OrderView(CRUDView):
    class Meta:
        model = Order
        name = "Order"
        schema = order_schema
        validate = validate_order
        validate_update = validate_order
