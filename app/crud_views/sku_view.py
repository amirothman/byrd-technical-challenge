from flask import jsonify
from sqlalchemy.exc import IntegrityError
from app.models import StorageKeepingUnit
from app.validators import validate_sku
from app import ma
from .crud_view import CRUDView


class SKUSchema(ma.ModelSchema):
    class Meta:
        model = StorageKeepingUnit
        fields = ("id", "product_name", "created_at", "updated_at")


sku_schema = SKUSchema()


class SKUView(CRUDView):
    class Meta:
        model = StorageKeepingUnit
        name = "SKU"
        schema = sku_schema
        validate = validate_sku
        validate_update = validate_sku

    def post(self):
        try:
            return super().post()
        except IntegrityError as err:
            return jsonify({
                "error": "Product name has to be unique. "
                "Stack trace: {}".format(str(err))
            }), 400
