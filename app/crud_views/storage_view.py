from flask import request
from marshmallow import fields
from app import ma, db
from app.models import Storage, StorageKeepingUnit
from app.validators import (
    validate_storage,
    validate_storage_update
)
from .crud_view import CRUDView


class StorageSchema(ma.ModelSchema):
    sku_id = fields.Integer(
        attribute="storage_keeping_unit_id",
    )

    class Meta:
        model = Storage
        fields = (
            "id", "sku_id", "sku",
            "stock", "created_at", "updated_at"
        )


storage_schema = StorageSchema()


class StorageView(CRUDView):
    class Meta:
        model = Storage
        name = "Storage"
        schema = storage_schema
        validate = validate_storage
        validate_update = validate_storage_update

    def post(self):
        error = self._meta.validate(request.json)
        if error:
            return error, 400
        storage = self._meta.schema.load(request.json)
        sku_id = storage.storage_keeping_unit_id
        if StorageKeepingUnit.query.get(sku_id):
            db.session.add(storage)
            db.session.commit()
            return self._meta.schema.jsonify(storage)
        else:
            return {
                "error": "SKU with ID {} does not exists".format(
                    sku_id
                )
            }, 400
