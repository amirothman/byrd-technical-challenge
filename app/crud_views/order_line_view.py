from marshmallow import fields
from flask import request
from app.models import OrderLine, StorageKeepingUnit
from app.validators import validate_order_line, validate_order_line_update
from app import ma, db
from .crud_view import CRUDView


class OrderLineSchema(ma.ModelSchema):
    sku_id = fields.Integer(
        attribute="storage_keeping_unit_id",
    )

    class Meta:
        model = OrderLine
        fields = (
            "id", "quantity", "sku_id",
            "sku", "created_at", "updated_at"
        )


order_line_schema = OrderLineSchema()


class OrderLineView(CRUDView):
    class Meta:
        model = OrderLine
        name = "Order Line"
        schema = order_line_schema
        validate = validate_order_line
        validate_update = validate_order_line_update

    def post(self):
        error = self._meta.validate(request.json)
        if error:
            return error, 400
        order_line = self._meta.schema.load(request.json)
        sku_id = order_line.storage_keeping_unit_id
        if StorageKeepingUnit.query.get(sku_id):
            db.session.add(order_line)
            db.session.commit()
            return self._meta.schema.jsonify(order_line)
        else:
            return {
                "error": "SKU with ID {} does not exists".format(
                    sku_id
                )
            }, 400
