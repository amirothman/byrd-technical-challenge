from flask import request
from flask.views import MethodView
from app import db


class CRUDView(MethodView):

    class Meta:
        model = None
        name = None
        schema = None
        validate = None
        validate_update = None

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        instance._meta = getattr(instance, 'Meta')
        return instance

    def get(self, id):
        if id is None:
            items = self._meta.model.query.all()
            return self._meta.schema.jsonify(items, many=True)
        else:
            item = self._meta.model.query.get(id)
            if item:
                return self._meta.schema.jsonify(item)
            else:
                return {
                    "error": "{} with ID {} not found".format(
                        self._meta.name,
                        id
                    )
                }, 400

    def post(self):
        error = self._meta.validate(request.json)
        if error:
            return error, 400
        item = self._meta.schema.load(request.json)
        db.session.add(item)
        db.session.commit()
        return self._meta.schema.jsonify(item)

    def delete(self, id):
        item = self._meta.model.query.get(id)
        if item:
            db.session.delete(item)
            db.session.commit()
            return ""
        else:
            return {
                "error": "{} with ID {} not found".format(
                    self._meta.name,
                    id
                )
            }, 400

    def put(self, id):
        item = self._meta.model.query.get(id)
        if item:
            self._meta.validate_update(request.json)
            for field, value in request.json.items():
                setattr(item, field, value)
            db.session.commit()
            return self._meta.schema.jsonify(item)
        else:
            return {
                "error": "{} with ID {} not found".format(
                    self._meta.name,
                    id
                )
            }, 400

    def patch(self, id):
        return self.put(id)
