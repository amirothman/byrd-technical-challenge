from logging import Formatter
from logging.handlers import RotatingFileHandler
from flask import Flask
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from config import environment_config

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()


def add_namespaces(api):
    from .orders import api as orders
    api.add_namespace(orders)

    from .order_lines import api as order_lines
    api.add_namespace(order_lines)

    from .storages import api as storages
    api.add_namespace(storages)

    from .storage_keeping_units import api as storage_keeping_units
    api.add_namespace(storage_keeping_units)


def set_logging_handler(app):
    handler = RotatingFileHandler(
        app.config["LOG_PATH"],
        maxBytes=512*1024*1024,
        backupCount=10
    )
    formatter = Formatter(
        "[%(levelname)s] %(asctime)s - %(name)s - %(message)s"
    )
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)


def register_api(app, view, endpoint, url, pk="id", pk_type="int"):
    view_func = view.as_view(endpoint)
    app.add_url_rule(
        url, defaults={pk: None},
        view_func=view_func, methods=("GET",)
    )
    app.add_url_rule(
        url, view_func=view_func,
        methods=("POST",)
    )
    app.add_url_rule(
        "%s<%s:%s>" % (url, pk_type, pk),
        view_func=view_func,
        methods=("GET", "PUT", "PATCH", "DELETE")
    )


def create_app(environment):
    app = Flask(
        __name__,
        instance_relative_config=True
    )
    app.config.from_object(environment_config[environment])
    set_logging_handler(app)
    app.url_map.strict_slashes = False
    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)
    from .crud_views import (
        SKUView,
        OrderView,
        OrderLineView,
        StorageView
    )
    register_api(app, SKUView, "sku", "/sku/")
    register_api(app, OrderView, "order", "/orders/")
    register_api(app, OrderLineView, "order_line", "/order_lines/")
    register_api(app, StorageView, "storage", "/storages/")

    from .fulfillments import fulfillments
    app.register_blueprint(fulfillments)

    return app
