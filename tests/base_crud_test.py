from uuid import UUID, uuid4
from random import randint
from app import create_app, db


class BaseCRUDTest:

    def set_model_specific_values(self):
        raise NotImplementedError("""
        Set the following values:
            * self.required_fields:[(str, type), ... ]
                - require fields with type (required)
            * self.endpoint:str
                - endpoint (required)
            * self.model:db.Model
                - model (required)
            * self.dependant_models:db.Model
                - dependant model if has relationships (required)
        """)

    def get_dummy_data(self, idx=""):
        d = {}
        for field, field_type in self.required_fields:
            if field_type is int:
                d[field] = randint(0, 100)
            if field_type is str:
                d[field] = "{}{}".format(field, idx)
            if field_type is UUID:
                d[field] = str(uuid4())
            if issubclass(field_type, db.Model):
                with self.app.app_context():
                    d[field] = field_type.query.first().id
        return d

    def setUp(self):

        self.set_model_specific_values()
        self.app = create_app('testing')
        with self.app.app_context():
            db.create_all()
        if self.dependant_models:
            with self.app.app_context():
                for model in self.dependant_models:
                    for i in range(10):
                        item = model()
                        db.session.add(item)
                db.session.commit()

        self.client = self.app.test_client
        for idx in range(100):
            d = self.get_dummy_data(idx=idx)
            self.client().post(
                self.endpoint,
                json=d
            )

    def get_first_item(self):
        res = self.client().get(self.endpoint)
        items = res.json
        return items[0]

    def get_first_item_id(self):
        item = self.get_first_item()
        return item["id"]

    def get_first_item_url(self):
        item_id = self.get_first_item_id()
        return "{}/{}".format(self.endpoint, item_id)

    def test_item_create_and_retrieve(self):
        item_data = self.get_dummy_data()
        with self.app.app_context():
            start_count = self.model.query.count()

        res = self.client().post(self.endpoint, json=item_data)
        self.assertEqual(res.status_code, 200)

        with self.app.app_context():
            self.assertEqual(self.model.query.count(), start_count+1)

        item_id = res.json["id"]
        res = self.client().get("{}/{}".format(self.endpoint, item_id))
        for field, _ in self.required_fields:
            self.assertEqual(res.json[field], item_data[field])

    def test_item_delete(self):

        item_url = self.get_first_item_url()
        res = self.client().delete(item_url)

        self.assertEqual(
            res.data.decode().replace("\n", ""), ""
        )
        self.assertEqual(res.status_code, 200)

        res = self.client().get(item_url)
        self.assertEqual(res.status_code, 400)
        self.assertIn("error", res.json)

        res = self.client().put(item_url)
        self.assertEqual(res.status_code, 400)
        self.assertIn("error", res.json)

    def test_item_update(self):
        item = self.get_first_item()
        updated_item = {}
        for field, field_type in self.required_fields:
            if field_type is int:
                updated_item[field] = randint(0, 100)
            if field_type is str:
                updated_item[field] = "updated{}".format(item[field])
            if field_type is UUID:
                updated_item[field] = str(uuid4())

        item_url = self.get_first_item_url()
        res = self.client().put(item_url, json=updated_item)
        self.assertEqual(res.status_code, 200)

        res = self.client().get(item_url)
        for field, _ in self.required_fields:
            if issubclass(field_type, db.Model):
                continue
            self.assertEqual(res.json[field], updated_item[field])

    def test_item_with_wrong_fields_should_fail(self):
        item_data = {
            "wrong_field": 1
        }
        res = self.client().post(self.endpoint, json=item_data)
        self.assertEqual(res.status_code, 400)
        self.assertIn("error", res.json)

    def test_item_with_additional_wrong_fields_should_fail(self):
        item_data = self.get_dummy_data()
        item_data["wrong_field"] = 1
        res = self.client().post(self.endpoint, json=item_data)
        self.assertEqual(res.status_code, 400)
        self.assertIn("error", res.json)

    def test_create_with_empty_payload_should_fail(self):
        res = self.client().post(self.endpoint, json={})
        self.assertEqual(res.status_code, 400)
        self.assertIn("error", res.json)

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()
