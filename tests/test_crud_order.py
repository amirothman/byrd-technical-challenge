from unittest import TestCase
from app.models import Order
from .base_crud_test import BaseCRUDTest


class CrudOrderTest(BaseCRUDTest, TestCase):
    def set_model_specific_values(self):
        self.required_fields = [("customer_name", str)]
        self.endpoint = "/orders"
        self.model = Order
        self.dependant_models = None
