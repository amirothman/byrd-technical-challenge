from unittest import TestCase
from app import create_app, db


class FulfillmentsTest(TestCase):

    def add_skus(self, count=5):
        for i in range(count):
            res = self.client().post(
                "/sku",
                json={"product_name": "product{}".format(i+1)}
            )
            self.skus.append(res.json)

    def add_storages(self, stock_iter=[10]):
        for sku in self.skus:
            sku_id = sku["id"]
            for stock in stock_iter:
                res = self.client().post(
                    "/storages",
                    json={
                        "sku_id": sku_id,
                        "stock": stock
                    }
                )
            self.storages.append(res.json)

    def setUp(self):
        self.app = create_app('testing')
        with self.app.app_context():
            db.create_all()
        self.client = self.app.test_client
        self.skus = []
        self.storages = []
        self.add_skus()
        self.add_storages()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def test_in_stock_sku_single_storage(self):
        self.client().post(
            "/order_lines",
            json={
                "quantity": 5,
                "sku_id": self.skus[0]["id"]
            }
        )
        res = self.client().get(
            "/fulfillments"
        )
        fulfillments = res.json
        fulfillment = fulfillments[0]
        self.assertEqual(1, len(fulfillments))
        self.assertIn("id", fulfillment)
        self.assertIn("quantity", fulfillment)

    def test_prioritize_storage_with_least_stock(self):
        res = self.client().post(
            "/storages",
            json={
                "stock": 5,
                "sku_id": self.skus[0]["id"]
            }
        )
        self.client().post(
            "/order_lines",
            json={
                "quantity": 5,
                "sku_id": self.skus[0]["id"]
            }
        )
        storage_with_less_stock = res.json
        res = self.client().get(
            "/fulfillments"
        )
        fulfillments = res.json
        fulfillment = fulfillments[0]
        self.assertEqual(fulfillment["id"], storage_with_less_stock["id"])

    def test_distributed_pickups(self):
        sku_id = self.storages[0]["sku_id"]

        storages = [self.storages[0]]
        res = self.client().post(
            "/storages",
            json={
                "stock": 3,
                "sku_id": sku_id
            }
        )
        storages.append(res.json)
        res = self.client().post(
            "/storages",
            json={
                "stock": 7,
                "sku_id": sku_id
            }
        )
        storages.append(res.json)
        sorted_storages = sorted(storages, key=lambda x: x["stock"])
        self.client().post(
            "/order_lines",
            json={
                "sku_id": sku_id,
                "quantity": 20
            }
        )
        res = self.client().get(
            "/fulfillments"
        )
        for fulfillment, storage in zip(res.json, sorted_storages):
            self.assertEqual(fulfillment["id"], storage["id"])

    def test_return_error_when_cannot_fulfill_order(self):
        sku_id = self.storages[0]["sku_id"]
        self.client().post(
            "/order_lines",
            json={
                "sku_id": sku_id,
                "quantity": 20
            }
        )
        res = self.client().get(
            "/fulfillments"
        )
        self.assertEqual(res.status_code, 400)
        self.assertIn("error", res.json)
