from unittest import TestCase
from app.models import OrderLine, StorageKeepingUnit
from .base_crud_test import BaseCRUDTest


class CrudOrderLineTest(BaseCRUDTest, TestCase):
    def set_model_specific_values(self):
        self.required_fields = [
            ("quantity", int),
            ("sku_id", StorageKeepingUnit)
        ]
        self.endpoint = "/order_lines"
        self.model = OrderLine
        self.dependant_models = [StorageKeepingUnit]
