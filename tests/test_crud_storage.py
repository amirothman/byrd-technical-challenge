from unittest import TestCase
from app.models import Storage, StorageKeepingUnit
from .base_crud_test import BaseCRUDTest


class CrudStorageTest(BaseCRUDTest, TestCase):
    def set_model_specific_values(self):
        self.required_fields = [
            ("stock", int),
            ("sku_id", StorageKeepingUnit)
        ]
        self.endpoint = "/storages"
        self.model = Storage
        self.dependant_models = [StorageKeepingUnit]
