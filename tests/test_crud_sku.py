from unittest import TestCase
from app.models import StorageKeepingUnit
from .base_crud_test import BaseCRUDTest


class CrudSKUTest(BaseCRUDTest, TestCase):

    def set_model_specific_values(self):
        self.required_fields = [("product_name", str)]
        self.endpoint = "/sku"
        self.model = StorageKeepingUnit
        self.dependant_models = None
